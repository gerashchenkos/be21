<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "shoppingcart" . DIRECTORY_SEPARATOR . "config.php";

if (!empty($_POST['products'])) {
    $_SESSION['products'] = $_POST['products'];
}

if (empty($_SESSION['products'])) {
    header("Location: index.php");
    die();
}

$allProducts = getProductsFromFile(ROOT_PATH . DIRECTORY_SEPARATOR . 'shoppingcart' . DIRECTORY_SEPARATOR . 'products.json');
$selectedProducts = getProductsDataByName($_SESSION['products'], $allProducts);

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'shoppingcart' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'cart.php';
