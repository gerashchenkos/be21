<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "shoppingcart" . DIRECTORY_SEPARATOR . "config.php";

$products = getProductsFromFile(ROOT_PATH . DIRECTORY_SEPARATOR . 'shoppingcart' . DIRECTORY_SEPARATOR . 'products.json');

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'shoppingcart' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'products.php';