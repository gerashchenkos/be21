<?php

function getProductsFromFile(string $fileName): array
{
    if (empty($fileName) || !file_exists($fileName)) {
        return [];
    }
    return json_decode(file_get_contents($fileName), true);
}

function getProductsDataByName(array $names, array $products): array
{
    return array_filter(
        $products,
        fn ($product) => in_array($product['name'], $names)
    );
}
