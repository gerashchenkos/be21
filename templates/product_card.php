<div class="card">
    <h1><?php echo $product['name'];?></h1>
    <p class="price">$<?php echo $product['price'];?></p>
    <p>Quantity: <?php echo $product['quantity'];?> SKU: <?php echo $product['sku'];?></p>
    <p><button>Add to Cart</button></p>
</div>
