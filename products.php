<?php

define('ROOT_PATH', dirname(__FILE__));
$products = [];
$titles = [];

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'utils' . DIRECTORY_SEPARATOR . 'get_products.php';

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'products.php';
?>