<?php

$text = "Главным фактором языка РНР является практичность. РНР должен предоставить программисту средства для быстрого и эффективного решения поставленных задач. Практический характер РНР обусловлен пятью важными характеристиками: традиционностью, простотой, эффективностью, безопасностью, гибкостью. Существует еще одна «характеристика», которая делает РНР особенно привлекательным: он распространяется бесплатно! Причем, с открытыми исходными кодами ( Open Source ). Язык РНР будет казаться знакомым программистам, работающим в разных областях. Многие конструкции языка позаимствованы из Си, Perl. Код РНР очень похож на тот, который встречается в типичных программах на С или Pascal. Это заметно снижает начальные усилия при изучении РНР. PHP — язык, сочетающий достоинства Perl и Си и специально нацеленный на работу в Интернете, язык с универсальным (правда, за некоторыми оговорками) и ясным синтаксисом. И хотя PHP является довольно молодым языком, он обрел такую популярность среди web-программистов, что на данный момент является чуть ли не самым популярным языком для создания web-приложений новых (скриптов).";
$maxLength = 80;

$words = explode(" ", $text);
$lines = [];
$i = 0;
$currentLineLength = 0;
foreach ($words as $word) {
    if (($currentLineLength + mb_strlen($word) + 1) > ($maxLength + 1)) {
        $currentLineLength = 0;
        $lines[$i]['length'] -= 1;
        $i += 1;
    }
    $lines[$i]['words'][] = $word;
    $currentLineLength += mb_strlen($word) + 1;
    $lines[$i]['length'] = $currentLineLength;
}
echo "<pre>";
//print_r($lines);
echo "</pre>";

$formattedLines = [];
$j = 0;
foreach ($lines as $line) {
    if ($j == (count($lines) - 1)) {
        $formattedLines[$j] = $line['words'];
        continue;
    }
    $addSpaces = $maxLength - $line['length'];
    for ($i = 0; $i < (count($line['words']) - 1); $i++) {
        if ($addSpaces == 0) {
            $formattedLines[$j][$i] = !empty($formattedLines[$j][$i]) ? $formattedLines[$j][$i] : $line['words'][$i];
        } else {
            $formattedLines[$j][$i] = !empty($formattedLines[$j][$i]) ? $formattedLines[$j][$i] . " " : $line['words'][$i] . " ";
            $addSpaces--;
        }
        if ($addSpaces > 0 && $i == (count($line['words']) - 2)) {
            $i = -1;
        }
    }
    $formattedLines[$j][count($line['words']) - 1] = $line['words'][count($line['words']) - 1];
    $j++;
    /*echo "<pre>";
    print_r($formattedLines);
    echo "</pre>";
    die();*/
}
foreach ($formattedLines as $line) {
    file_put_contents("strings.txt", implode(" ", $line) . PHP_EOL, FILE_APPEND);
}
