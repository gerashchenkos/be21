<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "shop_oop" . DIRECTORY_SEPARATOR . "config.php";

if (!empty($_SESSION['user'])) {
    header('Location: index.php');
    die();
}

$fields = ["full_name", "email", "password", "password_confirm"];
if (!empty($_POST)) {
    $errors = [];
    foreach ($fields as $field) {
        if (empty($_POST[$field])) {
            $errors[] = "Field " . ucwords(str_replace("_", " ", $field)) . " is required";
        }
    }

    if (!empty($_POST['email']) && User::checkIfUnique($_POST['email'], 'users', 'email')) {
        $errors[] = "Email must be unique";
    }

    if ($_POST['password'] !== $_POST['password_confirm']) {
        $errors[] = "Password must be confirmed";
    }

    if (empty($errors)) {
        //$userId = userRegistration($pdo, $_POST['full_name'], $_POST['email'], $_POST['password']);
        $user = User::save($_POST['full_name'], $_POST['email'], $_POST['password']);
        $_SESSION['user'] = User::getById($user->id);
        header('Location: index.php');
        die();
    }
}

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'shop_oop' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'registration.php';