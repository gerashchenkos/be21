<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "shop_oop" . DIRECTORY_SEPARATOR . "config.php";

use \ShopOpp\Classes\User;

if (!empty($_SESSION['user'])) {
    header('Location: index.php');
    die();
}

if (!empty($_POST['email']) && !empty($_POST['password'])) {
    //$user = checkLogin($pdo, $_POST['email'], $_POST['password']);
    $user = User::login($_POST['email'], $_POST['password']);
    $error = 0;
    if (!empty($user)) {
        $_SESSION['user'] = $user;
        header('Location: index.php');
        die();
    } else {
        $error = "Login credentials are invalid";
    }
}

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'shop_oop' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'login.php';