<?php

define('ROOT_PATH' , dirname(__FILE__, 2));
/*define('DB_USER' , 'db_user');
define('DB_PASS' , '1111');
define('DB_NAME' , 'test');
define('SALT' , '#ED$US3ert37mcd');*/
define('CLASS_PATH', dirname(__FILE__, 1) . DIRECTORY_SEPARATOR . "Classes");
define('INTERFACE_PATH', dirname(__FILE__, 1) . DIRECTORY_SEPARATOR . "Classes" . DIRECTORY_SEPARATOR . "Interfaces");

require_once ROOT_PATH . DIRECTORY_SEPARATOR . "shop_oop" . DIRECTORY_SEPARATOR . "functions.php";
require_once ROOT_PATH . DIRECTORY_SEPARATOR . "shop_oop" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

/*require_once CLASS_PATH . DIRECTORY_SEPARATOR . "Singleton.php";
require_once CLASS_PATH . DIRECTORY_SEPARATOR . "Db.php";
require_once INTERFACE_PATH . DIRECTORY_SEPARATOR . "CheckUnique.php";
require_once CLASS_PATH . DIRECTORY_SEPARATOR . "AbstractUser.php";
require_once CLASS_PATH . DIRECTORY_SEPARATOR . "User.php";
require_once CLASS_PATH . DIRECTORY_SEPARATOR . "Category.php";
require_once CLASS_PATH . DIRECTORY_SEPARATOR . "Product.php";*/

session_start();

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

/*function my_autoloader($className) {
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $namespace = trim($namespace, 'ShopOpp\\');
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    require $fileName;
}

spl_autoload_register('my_autoloader');*/
