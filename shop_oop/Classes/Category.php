<?php

namespace ShopOpp\Classes;

class Category
{
    protected string $id;
    protected string $title;
    protected string $status;

    public function __constructor()
    {

    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    public static function getAll(): array
    {
        $smtp = Db::getInstance()->pdo->prepare("
            SELECT
                *
            FROM
                `categories`
            "
        );
        $smtp->execute([]);
        $categoriesArray = $smtp->fetchAll();
        if (empty($categoriesArray)) {
            return [];
        }
        $categories = [];
        foreach ($categoriesArray as $category) {
            $cat = new Category();
            $cat->setId($category['id']);
            $cat->setTitle($category['title']);
            $cat->setStatus($category['status']);
            $categories[] = $cat;
        }
        return $categories;
    }
}