<?php

namespace ShopOpp\Classes;

use \ShopOpp\Classes\Interfaces\CheckUnique;

class User extends AbstractUser implements CheckUnique
{

    public function __construct(string $full_name, string $email, string|null $password = NULL, string|null $id = null, string $status = 'verified')
    {
        if (!empty($id)) {
            $this->id = $id;
        }
        $this->setFullName($full_name);
        if (empty($id)) {
            $this->setEmail($email);
            if (!empty($password)) {
                $this->setPassword($password);
            }
        } else {
            $this->email = $email;
            $this->password = $password;
        }
        $this->status = $status;
    }

    public function setFullName(string $name)
    {
        if($this->checkName($name)) {
            $this->full_name = $name;
        } else {
            throw new Exception('Name is invalid');
        }
    }

    public function setEmail(string $email)
    {
        if (self::checkIfUnique($email, "users", "email")) {
            throw new Exception('Field email must be unique');
        }
        $this->email = $email;
    }

    private function checkName(string $name): bool
    {
        if(strlen($name) >= 3 && strlen($name) < 150){
            return true;
        }
        return false;
    }

    protected function crypt(string $text)
    {
        return  password_hash($text, PASSWORD_DEFAULT);
    }

    public static function checkIfUnique(string $value, string $table, string $column): bool
    {
        // TODO: Implement checkIfUnique() method.
        $smtp = Db::getInstance()->pdo->prepare("
            SELECT
                `id`
            FROM
                `" . $table . "`
            WHERE
                `" . $column . "` = :value
        ");
        $smtp->execute(["value" => $value]);
        return (bool) $smtp->fetch();
    }

    public static function save(string $fullName, string $email, string $password): User
    {
        $user = new User($fullName, $email, $password);
        $smtp = Db::getInstance()->pdo->prepare("
            INSERT INTO `users` (
                `full_name`,
                `email`,
                `password`,
                `status`
            )
            VALUES
                (
                    :full_name,
                    :email,
                    :password,
                    :status
                )");
        $smtp->execute([
           "full_name" => $user->getFullName(),
           "email" => $user->getEmail(),
           "password" => $user->getPassword(),
           "status" => 'verified'
        ]);
        $user->id = Db::getInstance()->pdo->lastInsertId();
        return $user;

    }

    public static function getById(int $userId): User|null
    {
        if (empty($userId)) {
            return null;
        }
        $smtp = Db::getInstance()->pdo->prepare("SELECT * FROM `users` WHERE `id` = :id");
        $smtp->execute(["id" => $userId]);
        $userArray = $smtp->fetch();
        if (empty($userArray)) {
            return null;
        }
        $user =  new User(
            $userArray['full_name'],
            $userArray['email'],
            $userArray['password'],
            $userArray['id'],
            $userArray['status']
        );
        $user->setCreateAt($userArray['created_at']);
        return $user;
    }

    public static function login(string $email, string $password): User|null
    {
        $smtp = Db::getInstance()->pdo->prepare(
            "
                SELECT
                    `users`.*
                FROM
                    `users`
                WHERE
                    `email` = :email
            "
        );
        $smtp->execute(["email" => $email]);
        $userArray = $smtp->fetch();
        if (password_verify($password, $userArray['password'])) {
            $user =  new User(
                $userArray['full_name'],
                $userArray['email'],
                $userArray['password'],
                $userArray['id'],
                $userArray['status']
            );
            $user->setCreateAt($userArray['created_at']);
            return $user;
        }
        return null;
    }
}