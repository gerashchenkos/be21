<?php

namespace ShopOpp\Classes;

abstract class AbstractUser
{
    public string|null $id = null;
    protected string $full_name;
    protected string $password;
    protected string $email;
    protected string $status;
    protected string $created_at;

    public function setFullName(string $name)
    {
        $this->full_name = $name;
    }

    public function getFullName(): string
    {
        return $this->full_name;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getCreateAt()
    {
        return $this->created_at;
    }

    public function setCreateAt(string $createAt)
    {
        $this->created_at = $createAt;
    }

    public function setPassword(string $password)
    {
        return $this->password = $this->crypt($password);
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    protected abstract function crypt(string $text);
}