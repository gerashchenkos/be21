<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "shop_oop" . DIRECTORY_SEPARATOR . "config.php";

//TEMPORARY SOLUTION UNTIL WE WILL ADD LOGIN PAGE
//$_SESSION['user_id'] = 1;

if (!empty($_POST['products'])) {
    if (empty($_SESSION['cart_id'])) {
        $cartId = createCart($pdo, $_SESSION['user']['id']);
        if(!empty($cartId)){
            $_SESSION['cart_id'] = $cartId;
        }
    }
    addProductsToCart($pdo, $_SESSION['cart_id'], $_POST['products']);
}

if (empty($_SESSION['cart_id'])) {
    header("Location: index.php");
    die();
}

$selectedProducts = getProductsFromCart($pdo, $_SESSION['cart_id']);

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'shop_oop' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'cart.php';
