<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "shop_oop" . DIRECTORY_SEPARATOR . "config.php";

use \ShopOpp\Classes\Product;
use \ShopOpp\Classes\Category;

if (empty($_SESSION['user'])) {
    header('Location: login.php');
    die();
}

if (!empty($_GET['cat_id'])) {
    //$products = getProductsByGategory($pdo, (int)$_GET['cat_id']);
    $products = Product::getByField('category_id', (int)$_GET['cat_id']);
} else {
    //$products = getProducts($pdo);
    $products = Product::getAll();
}
$categories = Category::getAll();

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'shop_oop' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'products.php';