CREATE TABLE `users`
(
    `id`         int unsigned NOT NULL AUTO_INCREMENT,
    `full_name`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `email`      varchar(80)                                             NOT NULL,
    `password`   varchar(100)                                            NOT NULL,
    `status`     enum('not_verified','verified','banned') NOT NULL DEFAULT 'not_verified',
    `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;

CREATE TABLE `products`
(
    `id`          int unsigned NOT NULL AUTO_INCREMENT,
    `sku`         varchar(20)   NOT NULL,
    `category_id` int unsigned NOT NULL,
    `title`       varchar(120)  NOT NULL,
    `description` text          NOT NULL,
    `price`       decimal(8, 2) NOT NULL,
    `quantity`    int unsigned NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;

CREATE TABLE `categories`
(
    `id`     int unsigned NOT NULL AUTO_INCREMENT,
    `title`  varchar(120) NOT NULL,
    `status` enum('active','inactive') NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;

CREATE TABLE `cart`
(
    `id`         int unsigned NOT NULL AUTO_INCREMENT,
    `user_id`    int unsigned NOT NULL,
    `product_id` int unsigned NOT NULL,
    `quantity`   int unsigned NOT NULL,
    `ordered_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;

ALTER TABLE `cart` DROP COLUMN `product_id`;
ALTER TABLE `cart` DROP COLUMN `quantity`;

CREATE TABLE `cart_products`
(
    `cart_id`    int unsigned NOT NULL,
    `product_id` int unsigned NOT NULL,
    `quantity`   int unsigned NOT NULL,
    PRIMARY KEY (`cart_id`, `product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;


