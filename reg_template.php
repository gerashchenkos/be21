<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Home Work</title>
</head>

<body>

<form action="#" method="post">
    <input type="text" value="<?php echo $name ?>">
    <input type="text" value="<?php echo $login ?>">
    <input type="text" value="<?php echo $email ?>">
    <input type="password" value="<?php echo $password ?>">

    <select name="" id="">
        <option value="<?php echo current($langKeys); ?>"><?php echo current($lang); ?></option>
        <option value="<?php echo next($langKeys); ?>"><?php echo next($lang); ?></option>
        <option value="<?php echo next($langKeys); ?>"><?php echo next($lang); ?></option>
        <option value="<?php echo next($langKeys); ?>"><?php echo next($lang); ?></option>
    </select>

    <input type="submit" value="Войти">
</form>
</body>
</html>
