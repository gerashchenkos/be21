<?php

function generateSignature($login, $time)
{
    $stringToSign =
        $login . "\n" .
        $time . "\n" .
        $_SERVER['HTTP_USER_AGENT'] . "\n" .
        $_SERVER['REMOTE_ADDR'];

    return hash_hmac('SHA1', $stringToSign, SECRET);
}