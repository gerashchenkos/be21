<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "session" . DIRECTORY_SEPARATOR . "config.php";

$isLogged = 0;
if (!empty($_POST['login']) && !empty($_POST['password'])) {
    $userData = json_decode(file_get_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "homework_files" . DIRECTORY_SEPARATOR . "login.json"), true);
    foreach ($userData as $user) {
        if($user['login'] === $_POST['login'] && $user['pass'] === $_POST['password']){
            $isLogged = 1;
            $_SESSION['login'] = $user['login'];
            if (!empty($_POST['remember_me'])) {
                $time = time();
                $coolie = $user['login'] . ':' . time() . ':' . generateSignature($user['login'], $time);
                setcookie("login", $coolie, ["expires" => time() + 3600]);
            }
            header("Location: home.php");
            break;
        }
    }
    $isLogged = $isLogged ? $isLogged : -1;
}
var_dump($isLogged);
include_once ROOT_PATH . DIRECTORY_SEPARATOR . "session" . DIRECTORY_SEPARATOR . "login_template.php";
?>
