<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "session" . DIRECTORY_SEPARATOR . "config.php";

if (empty($_SESSION['login'])) {
    if (!empty($_COOKIE['login'])) {
        list($login, $time, $signature) = explode(':',$_COOKIE['login']);
        if($signature === generateSignature($login, $time)) {
            $_SESSION['login'] = $login;
        }
    }
    if (empty($_SESSION['login'])) {
        header("Location: login.php");
        die();
    }
}

echo "Hello, " . $_SESSION['login'] ."!";

//session_destroy();

