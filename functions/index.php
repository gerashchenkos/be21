<?php

$a = 1.4;
$b = 2;
$c = 2;
echo calc($a, $b);
$a = 1;
//$aa = recursion($a);
//var_dump($aa);

$users = [
    ["login" => "yura", "pass" => "11"],
    ["login" => "ivan", "pass" => "123456"],
    ["login" => "denis", "pass" => "1121989"]
];

function filter($user)
{
    return ["login" => $user['login'], "pass" => str_repeat("*", strlen($user['pass']))];
}

$separator = '#';
$filteredUsers = array_map(
    function ($user) use ($separator) {
        return [
            "login" => $user['login'],
            "pass" => str_repeat($separator, strlen($user['pass']))
        ];
    }, $users);
echo "<pre>";
print_r($filteredUsers);
echo "</pre>";

$filteredUsers = array_map(
    fn ($user) =>
        [
            "login" => $user['login'],
            "pass" => str_repeat($separator, strlen($user['pass']))
        ],
    $users
);
echo "<pre>";
print_r($filteredUsers);
echo "</pre>";

function calc(float $param1, float $param2, string $action = "+"): float
{
    $res = 0;
    switch ($action){
        case "+" : $res = $param1 + $param2;
                    break;
        case "-" : $res = $param1 - $param2;
            break;
        case "*" : $res = $param1 * $param2;
            break;
        case "/" : $res = $param1 / $param2;
            break;
    }
    return $res;
}

function recursion($a)
{
    if ($a < 20) {
        echo "$a\n";
        return recursion($a + 1);
    } else {
        return 20;
    }
}
