<?php
define('ROOT_PATH' , dirname(__FILE__, 2));
define('PERC', 10);

if (!empty($_POST['amount']) && !empty($_POST['term'])) {
    $revenue = calculateDeposit($_POST['amount'], $_POST['term'], PERC, $_POST['capitalization'] ?? 0);
}

function calculateDeposit(float $amount, float $term, float $perc, int $capitalization = 0): float
{
    $percAmount = 0;
    for ($i = 1; $i <= $term; $i++) {
        $monthPerc = $amount * (PERC / 12 / 100);
        $percAmount += $monthPerc;
        $amount = $capitalization ? $amount + $monthPerc : $amount;
    }
    return $percAmount;
}

include_once ROOT_PATH . DIRECTORY_SEPARATOR . "functions" . DIRECTORY_SEPARATOR . "deposits_template.php";
?>