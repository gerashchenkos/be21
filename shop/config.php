<?php

define('ROOT_PATH' , dirname(__FILE__, 2));
define('DB_USER' , 'db_user');
define('DB_PASS' , '1111');
define('DB_NAME' , 'test');
define('SALT' , '#ED$US3ert37mcd');

require_once ROOT_PATH . DIRECTORY_SEPARATOR . "shop" . DIRECTORY_SEPARATOR . "functions.php";

session_start();

$dsn = "mysql:host=localhost;port=3306;dbname=" . DB_NAME . ";charset=utf8";

$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
];

$pdo = new PDO($dsn, DB_USER, DB_PASS, $options);
