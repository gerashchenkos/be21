<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "shop" . DIRECTORY_SEPARATOR . "config.php";

if (empty($_SESSION['user'])) {
    header('Location: login.php');
    die();
}

if (!empty($_GET['cat_id'])) {
    $products = getProductsByGategory($pdo, (int)$_GET['cat_id']);
} else {
    $products = getProducts($pdo);
}
$categories = getAllCategories($pdo);

require_once ROOT_PATH . DIRECTORY_SEPARATOR . 'shop' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'products.php';