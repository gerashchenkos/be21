<?php require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "header.php");?>

<main>

    <section class="py-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Shopping Cart example</h1>
                <p class="lead text-muted">PHP Shopping Cart</p>
                <p>
                    <a href="#" class="btn btn-secondary my-2">Products Page</a>
                    <a href="#" class="btn btn-primary my-2">Cart Page</a>
                </p>
            </div>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <form method="POST" action="/shoppingcart/cart.php">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                <?php foreach($selectedProducts as $product): ?>
                    <div class="col">
                        <div class="card shadow-sm">
                            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em"><?php echo $product['title'];?></text></svg>

                            <div class="card-body">
                                <p class="card-text"><?php echo $product['description'] . " " . "$ " . $product['price'];?></p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                    </div>
                                    <small class="text-muted">Quantity: 1</small>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
                <br>
                <div class="d-flex justify-content-center">
                    <!--<button type="submit" class="btn btn-primary">Buy</button>-->
                </div>
            </form>


        </div>
    </div>

</main>

<?php require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "footer.php");?>
