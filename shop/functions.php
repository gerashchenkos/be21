<?php

function getProducts(object $pdo): array
{
    if (empty($pdo)) {
        return [];
    }
    $smtp = $pdo->prepare("SELECT * FROM `products`");
    $smtp->execute([]);
    return $smtp->fetchAll();
}

function createCart(object $pdo, string $userId): int
{
    if (empty($pdo) || empty($userId)) {
        return 0;
    }
    $smtp = $pdo->prepare("INSERT INTO `cart`(`user_id`) VALUES(:user_id)");
    $smtp->execute(["user_id" => $userId]);
    return $pdo->lastInsertId();
}

function addProductsToCart(object $pdo, int $cartId, array $products): bool
{
    if (empty($pdo) || empty($cartId) || empty($products)) {
        return false;
    }
    foreach ($products as $product) {
        $isProductAdded = getProductFromCart($pdo, $cartId, $product);
        if (!empty($isProductAdded)) {
            //update product quantity
        } else {
            $smtp = $pdo->prepare("
                INSERT INTO `cart_products` (
                    `cart_id`,
                    `product_id`,
                    `quantity`
                )
                VALUES
                (
                    :cart_id,
                    :product_id,
                    :quantity
                )");
            $smtp->execute([
                "cart_id" => $cartId,
                "product_id" => $product,
                "quantity" => 1
            ]);
        }
    }
    return true;
}

function getProductFromCart(object $pdo, int $cartId, int $productId): array
{
    if (empty($pdo) || empty($cartId) || empty($productId)) {
        return [];
    }
    $smtp = $pdo->prepare("
        SELECT
            *
        FROM
            `cart_products`
        WHERE
            `cart_id` = :cart_id
        AND `product_id` = :product_id
    ");
    $smtp->execute([
        "cart_id" => $cartId,
        "product_id" => $productId
    ]);
    return !($product = $smtp->fetch()) ? [] : $product;
}

function getProductsFromCart(object $pdo, int $cartId): array
{
    if (empty($pdo) || empty($cartId)) {
        return [];
    }
    $smtp = $pdo->prepare("
        SELECT
            `products`.*,
            `cart_products`.`quantity` as selected_quantity
        FROM
            `cart_products`
        INNER JOIN `products` ON `products`.`id` = `cart_products`.`product_id`
        WHERE
            `cart_id` = :cart_id
    ");
    $smtp->execute([
       "cart_id" => $cartId,
    ]);
    return !($products = $smtp->fetchAll()) ? [] : $products;
}

function getAllCategories(object $pdo): array
{
    if (empty($pdo)) {
        return [];
    }
    $smtp = $pdo->prepare("
        SELECT
            *
        FROM
            `categories`
    ");
    $smtp->execute([
    ]);
    return !($categories = $smtp->fetchAll()) ? [] : $categories;
}

function getProductsByGategory(object $pdo, int $categoryId): array
{
    if (empty($pdo) || empty($categoryId)) {
        return [];
    }
    $smtp = $pdo->prepare("
        SELECT
            `products`.*
        FROM
            `products` INNER JOIN `categories` ON `categories`.`id` = `products`.`category_id`
        WHERE
            `category_id` = :category_id
    ");
    $smtp->execute(["category_id" => $categoryId]);
    return $smtp->fetchAll();
}

function checkLogin(object $pdo, string $email, string $password): array
{
    if (empty($pdo) || empty($email) || empty($password)) {
        return [];
    }
    $smtp = $pdo->prepare("
        SELECT
            `users`.*
        FROM
            `users`
        WHERE
            `email` = :email
    ");
    $smtp->execute(["email" => $email]);
    $user = $smtp->fetch();
    if (password_verify($password, $user['password'])) {
       return $user;
    }
    return [];
}

function checkEmailUnique(object $pdo, string $email): bool
{
    $smtp = $pdo->prepare("
        SELECT
            `id`
        FROM
            `users`
        WHERE
            `email` = :email
    ");
    $smtp->execute(["email" => $email]);
    return (bool) $smtp->fetch();
}

function userRegistration(object $pdo, string $fullName, string $email, string $password): string
{
    $smtp = $pdo->prepare("
        INSERT INTO `users` (
            `full_name`,
            `email`,
            `password`,
            `status`
        )
        VALUES
            (
                :full_name,
                :email,
                :password,
                :status
            )");
    $smtp->execute([
        "full_name" => $fullName,
        "email" => $email,
        "password" => password_hash($password, PASSWORD_DEFAULT),
        "status" => 'verified'
    ]);
    return $pdo->lastInsertId();
}

function getUserById(object $pdo, int $userId): array
{
    if (empty($pdo) || empty($userId)) {
        return [];
    }
    $smtp = $pdo->prepare("SELECT * FROM `users` WHERE `id` = :id");
    $smtp->execute(["id" => $userId]);
    return !($user = $smtp->fetch()) ? [] : $user;
}

function dd($val)
{
    echo "<pre>";
    print_r($val);
    echo "</pre>";
    die();
}
