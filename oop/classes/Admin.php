<?php


class Admin extends User
{
    public static string $type;

    public function __construct(string $name, string $email, string|null $password = NULL)
    {
        parent::__construct($name, $email, $password);
        //self::$type = 'admin';
        self::changeType("admin");
    }

    public function changeStatus(User $user, string $status)
    {
        if(!($user instanceof Admin)) {
            $user->status = $status;
        }
    }

    public static function changeType(string $type)
    {
        self::$type = $type;
    }
}