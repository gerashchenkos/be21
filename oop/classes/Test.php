<?php


class Test
{
    protected string $name = "Test";
    private string $password;
    private string $email;
    private const SALT = '12dcw$ndhTR';

    public function __construct(string $name, string|null $password = NULL)
    {
        $this->setName($name);
        if (!empty($password)) {
            $this->setPassword($password);
        }
    }

    public function setName(string $name)
    {
        if($this->checkName($name)) {
            $this->name = $name;
        } else {
            throw new Exception('Name is invalid');
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        $emailParts  = explode("@", $this->email);
        $email = $emailParts[0][0] . "***" . $emailParts[0][strlen($emailParts[0]) - 1] . "@" . $emailParts[1];
        return $email;
    }

    public function setPassword(string $password)
    {
        $this->password = $this->crypt($password);
    }

    private function checkName(string $name): bool
    {
        if(strlen($name) >= 3 && strlen($name) < 150){
            return true;
        }
        return false;
    }

    private function crypt(string $text)
    {
        return sha1($text . self::SALT);
    }

    public function __destruct()
    {
        echo "Класс уничтожен!";
    }

    public function __call($name, $arguments)
    {
        // Замечание: значение $name регистрозависимо.
        if (in_array($name, ['setEmail'])) {
            $this->email = $arguments[0];
        }
    }

    public function __get(string $name): mixed
    {
        /*$privateProperties = ["email", "name"];
        if (in_array($name, $privateProperties)) {
            return $this->{"get" . ucfirst($name)}(); // $this->getName() or $this->getEmail()
        }*/
        if (method_exists('Test', "get" . ucfirst($name))) {
            return $this->{"get" . ucfirst($name)}();
        }
        return null;
    }

    public function __set(string $name, mixed $value): void
    {
        if (method_exists('Test', "set" . ucfirst($name))) {
            $this->{"set" . ucfirst($name)}($value);
        }
    }

    public function __sleep()
    {
        return array('name', 'email');
    }

    public function __toString(): string
    {
        return $this->getName() . ' ' . $this->getEmail();
    }

}