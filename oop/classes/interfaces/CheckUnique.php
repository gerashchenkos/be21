<?php

interface CheckUnique
{
    public function checkIfUnique(string $value, string $table, string $column): bool;
}
