<?php


abstract class AbstractUser
{
    public function setPassword(string $password)
    {
        $this->password = $this->crypt($password);
    }

    protected abstract function crypt(string $text);
}