<?php


class User extends AbstractUser implements CheckUnique
{
    protected string $name;
    protected string $password;
    protected string $email;
    protected string $status;

    public function __construct(string $name, string $email, string|null $password = NULL)
    {
        $this->setName($name);
        $this->email = $email;
        if (!empty($password)) {
            $this->setPassword($password);
        }
    }

    public function setName(string $name)
    {
        if($this->checkName($name)) {
            $this->name = $name;
        } else {
            throw new Exception('Name is invalid');
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    private function checkName(string $name): bool
    {
        if(strlen($name) >= 3 && strlen($name) < 150){
            return true;
        }
        return false;
    }

    protected function crypt(string $text)
    {
        return  password_hash($text, PASSWORD_DEFAULT);
    }

    public function checkIfUnique(string $value, string $table, string $column): bool
    {
        // TODO: Implement checkIfUnique() method.
        return true;
    }
}