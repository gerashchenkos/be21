<?php

define('ROOT_PATH' , dirname(__FILE__, 2));
define('UPLOADS_PATH' , ROOT_PATH . DIRECTORY_SEPARATOR . "uploads" .  DIRECTORY_SEPARATOR);

/*$users = [
    ["login" => "yura", "pass" => "11"],
    ["login" => "ivan", "pass" => "123456"],
    ["login" => "denis", "pass" => "1121989"]
];
file_put_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "homework_files" . DIRECTORY_SEPARATOR . "login.json", json_encode($users));
*/

$isLogged = 0;
if (!empty($_POST['login']) && !empty($_POST['password'])) {
    $uploadfile = UPLOADS_PATH . basename($_FILES['userfile']['name']);
    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
        echo "Файл корректен и был успешно загружен.\n";
    } else {
        echo "Возможная атака с помощью файловой загрузки!\n";
    }
    //----- with text file
    /*$fp = fopen( ROOT_PATH . DIRECTORY_SEPARATOR . "homework_files" . DIRECTORY_SEPARATOR . "login.txt", "r");
    if (!$fp) {
        die("File Error!");
    }
    while (!feof($fp)) {
        $userData = explode(" " , trim(fgets($fp)));
        if ($_POST['login'] === $userData[0] && $_POST['password'] === $userData[1]) {
            $isLogged = 1;
            $userFile = ROOT_PATH . DIRECTORY_SEPARATOR . "homework_files" . DIRECTORY_SEPARATOR . $_POST['login'] . ".txt";
            if (file_exists(
                $userFile
            )) {
                $successLogged = (int) file_get_contents($userFile);
                $successLogged ++;
                file_put_contents($userFile, $successLogged);
            } else {
                file_put_contents($userFile, 1);
            }
            break;
        }
    }
    fclose($fp);*/
    //-----------

    $userData = json_decode(file_get_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "homework_files" . DIRECTORY_SEPARATOR . "login.json"), true);
    foreach ($userData as $user) {
        if($user['login'] === $_POST['login'] && $user['pass'] === $_POST['password']){
            $isLogged = 1;
            $userFile = ROOT_PATH . DIRECTORY_SEPARATOR . "homework_files" . DIRECTORY_SEPARATOR . $_POST['login'] . ".json";
            if (file_exists(
                $userFile
            )) {
                $successLogged = (int) json_decode(file_get_contents($userFile), true)['succeess_logins'];
                $successLogged ++;
                file_put_contents($userFile, json_encode(["succeess_logins" => $successLogged]));
            } else {
                file_put_contents($userFile, json_encode(["succeess_logins" => 1]));
            }
            break;
        }
    }

    $isLogged = $isLogged ? $isLogged : -1;
}

if ($isLogged === 1) {
    echo "Hello, " . $_POST['login'];
} else {
    include_once ROOT_PATH . DIRECTORY_SEPARATOR . "homework_files" . DIRECTORY_SEPARATOR . "login_template.php";
}
?>
