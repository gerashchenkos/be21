<?php
    define('ROOT_PATH' , dirname(__FILE__));
    $f = fopen( ROOT_PATH . DIRECTORY_SEPARATOR . "file.txt", "r+");
    if (!$f) {
        die("We can't open file!");
    }
    /*while (!feof($f)) {
        echo fgets($f) . "<br>";
    }*/
    fseek($f, 4);
    fputs($f, '!!!!');
    rewind($f);
    /*while (!feof($f)) {
        echo fgets($f) . "<br>";
    }*/
    fclose($f);

    $text = file_get_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "file.txt");
    //var_dump($text);
    file_put_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "file2.txt", "!!New text", );
    $row = 1;
    $products = [];
    $titles = [];
    if (($handle = fopen("test.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
            if ($row > 1) {
                $products[] = ["name" => $data[0], "price" => $data[1], "quantity" => $data[2], "sku" => $data[3]];
            } else{
                $titles = [$data[0], $data[1], $data[2], $data[3]];
            }
            $row++;
        }
        fclose($handle);
    }
    $productJson = json_encode($products);
    file_put_contents("products.json", $productJson);
    if (file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . "products.json")) {
        $product = json_decode(file_get_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "products.json"), true);
    }
    //var_dump($product);
    header('Content-Type: application/json');
    echo $productJson;
    die();
?>
<html>
<head>
</head>
<body>
    <table>
        <tr>
            <?php foreach($titles as $title): ?>
                <th><?php echo $title;?></th>
            <?php endforeach; ?>
        </tr>
        <?php foreach($products as $product): ?>
            <tr>
                <?php foreach($product as $val): ?>
                    <td><?php echo $val;?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
    </table>
</body>
</html>
